# Jobeet-Symfony #

This project is for learning the bases of the PHP framework Symfony2.

It begins with the install of Symfony2 in this origin version.
Then we follow the tutorial of Jobeet, on this link :
[http://www.ens.ro/2012/03/21/jobeet-tutorial-with-symfony2/](http://www.ens.ro/2012/03/21/jobeet-tutorial-with-symfony2/)

Then we need modify some files or code which is depreciated or obsolete.

Enjoy it !

*Done by Maxime S. for the IMIE Rennes*